nextflow.enable.dsl=2
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                           JASS suite pipeline
          Authors : Hanna Julienne, Hervé Ménager & Lucie Troubat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

/* Parameter to set if optional pipeline steps are performed */
params.compute_project=true // Compute JASS runs
params.compute_LDSC_matrix=false // Infer the genetic covariance and residual covariance using the LDscore regression (Bulik-Sullivan, et al, 2015). The residual covariance is necessary to perform multi-trait GWAS (see julienne, et al 2021) If set to false, the residual covariance will be infered from Zscores
params.compute_imputation=false

/* Path of input data */
params.meta_data = "${projectDir}"+"/input_files/Data_test_EAS.csv" 
 // "${projectDir}"+"/input_files/Meta_data_preliminary_analysis.csv" file describing gwas summary statistic format
params.gwas_folder = "${projectDir}"+'/test_data/hg38_EAS/' 

params.region = Channel.fromPath("${projectDir}"+"/input_files/All_Regions_ALL_ensemble_1000G_hg38_EAS.bed")
params.ref_panel_WG = "${projectDir}"+"/Ref_Panel/1000G_EAS_0_01_chr22_21.csv"


params.ancestry="EAS"
params.prefix="ALL_ensemble_1000G_hg38_EAS_chr"
params.prefix_Impute_GWAS="ALL_ensemble_1000G_hg38_EAS_"
params.suffix=""
params.LD_SCORE_folder='/pasteur/zeus/projets/p02/GGS_WKD/DATA_1000G/Panels/Matrix_LDSCORE/EAS/'

/* Folder to store result output */
params.output_folder = "${projectDir}"
params.diagnostic_folder= params.output_folder + "/sample_size/"

/**********************/
/* Process Parameters */
/**********************/
/* Imputation parameter (for RAISS) */
/* If you decide to run RAISS, we strongly encourage you to tune 
    the following parameters to your data to ensure imputation accuracy.
    see https://statistical-genetics.pages.pasteur.fr/raiss/#optimizing-raiss-parameters-for-your-data
*/
params.r2threshold = 0.6
params.eigenthreshold = 0.05 
params.minimumld = 5
params.ld_type = "plink"
params.ld_folder = Channel.fromPath("/pasteur/zeus/projets/p02/GGS_WKD/DATA_1000G/Panels/Matrix_LD_RAISS/EAS/*.ld").collect()
params.ref_panel = '/pasteur/zeus/projets/p02/GGS_JASS/jass_analysis_pipeline/Ref_panel_by_chr/'
chr_channel = Channel.from(1..22)
params.ref_chr_path= Channel.fromPath("/pasteur/zeus/projets/p02/GGS_WKD/DATA_1000G/Panels/EAS/ALL_ensemble_1000G_hg38_EAS_chr*.bim").collect()

params.perform_sanity_checks=false
params.perform_accuracy_checks=false

/* Project group */
params.group = "${projectDir}/input_files/group.txt"
group = file(params.group)
extract_sample_size_script_channel = "${projectDir}/bin/extract_sample_size.py"
generate_trait_pairs_channel = "${projectDir}/bin/generate_trait_pairs.py"
parse_correlation_channel = "${projectDir}/bin/parse_correlation_results.py"
make_heatmap_channel = "${projectDir}/bin/make_heatmap.R"

/* current date */
def today = new Date().format('yyyy-MM-dd')
params.current_date = today

/*****************************/
/*   process inclusion   */
/*****************************/
include {Meta_data_GWAS; Clean_GWAS} from "./modules/Clean_GWAS"
include {Impute_GWAS} from "./modules/RAISS_imputation"
include {Perf_RAISS; Sanity_Checks_RAISS; Put_in_harmonized_folder; Put_in_imputed_folder} from "./modules/RAISS_sanity_checks"
include {Munge_LDSC_data; Heritability_LDSC_data;Generate_trait_pair; Correlation_LDSC_data;Parsing_correlation_matrices; Make_HeatMap} from "./modules/LDSC" //;  Generate_trait_pair;
include {Create_inittable_LDSC; Create_inittable; Get_pheno_group; Create_project_data} from "./modules/JASS"


/*******************************/
workflow{
    /****** PREPROCESSING ******/
    Meta_data_GWAS(params.meta_data)
    Clean_GWAS(params.ref_panel_WG, Meta_data_GWAS.out.flatten())

    /******** IMPUTATION *******/
    if(params.compute_imputation){

        Impute_GWAS(Clean_GWAS.out.cleaned_gwas_chr_channel.flatten(), params.ref_chr_path.collect(), params.ld_folder.collect())

        sanity_cleaned_in_folder = Put_in_harmonized_folder(Clean_GWAS.out.cleaned_gwas_chr_channel.collect())
        sanity_imputed_in_folder = Put_in_imputed_folder(Impute_GWAS.out.imputed_gwas_channel.collect())

        chr_channel
        .max()
        .map{it + 1}
        .set {chr_max}

        chr_channel
        .min()
        .set {chr_min}

        Clean_GWAS.out.cleaned_gwas_chr_channel
            .map {(it.name.toString() =~ /(z_[0-9A-Za-z-]+_[0-9A-Za-z-]+)/ )[0][1]}
            .unique()
            .set {trait_channel}

        Clean_GWAS.out.cleaned_gwas_chr_channel.flatten().filter( ~/.*z.*chr22\.txt/ ).view()
        if(params.perform_accuracy_checks){
            Perf_RAISS(Clean_GWAS.out.cleaned_gwas_chr_channel.flatten().filter( ~/.*z.*_chr22\.txt/ ), params.ref_chr_path.collect(), params.ld_folder.collect())
        }

        if(params.perform_sanity_checks){
            Sanity_Checks_RAISS(sanity_cleaned_in_folder.collect(), sanity_imputed_in_folder.collect(), trait_channel, chr_min, chr_max)
        }
    }

    // /****** Genetic variance and corresponding inittable ******/

    if(params.compute_LDSC_matrix){
        Munge_LDSC_data(Clean_GWAS.out.cleaned_gwas_channel.flatten())
        Heritability_LDSC_data(Munge_LDSC_data.out.ldsc_data_channel)
        Generate_trait_pair(generate_trait_pairs_channel, Munge_LDSC_data.out.ldsc_data_channel.collect())  
        Correlation_LDSC_data(Generate_trait_pair.out.flatten(), Munge_LDSC_data.out.ldsc_data_channel.collect())
        Parsing_correlation_matrices(parse_correlation_channel, Correlation_LDSC_data.out.cor_log_channel.collect(), Heritability_LDSC_data.out.h2_log_channel.collect())
	Make_HeatMap(make_heatmap_channel, Parsing_correlation_matrices.out.parsing_results.collect())

        /****** JASS Inittable with LDSC covariance matrix (preferred method) *****/
        if(params.compute_imputation){
            Create_inittable_LDSC(Impute_GWAS.out.imputed_gwas_channel.collect(), Clean_GWAS.out.cleaned_gwas_channel.collect(), Parsing_correlation_matrices.out.cov_H0_matrice_channel, Parsing_correlation_matrices.out.cov_gen_matrice_channel, params.region)
        }
        else{
            Create_inittable_LDSC(Clean_GWAS.out.cleaned_gwas_chr_channel.collect(), Clean_GWAS.out.cleaned_gwas_channel.collect(), Parsing_correlation_matrices.out.cov_H0_matrice_channel, Parsing_correlation_matrices.out.cov_gen_matrice_channel, params.region)
        }

        /*Compute multi-trait GWAS*/
        if(params.compute_project){
            Get_pheno_group(params.group)
            Create_project_data(Get_pheno_group.out.pheno_group, Create_inittable_LDSC.out.init_table)
        }
    }
    else {
            if(params.compute_imputation){
                Create_inittable(Impute_GWAS.out.imputed_gwas_channel.collect(), Clean_GWAS.out.cleaned_gwas_channel.collect(), params.region)
            }
            else{
                Create_inittable(Clean_GWAS.out.cleaned_gwas_chr_channel.collect(), Clean_GWAS.out.cleaned_gwas_channel.collect(), params.region)
            }

            /*Compute multi-trait GWAS*/
            if(params.compute_project){
                Get_pheno_group(params.group)
                Get_pheno_group.out.pheno_group.flatten().view()
                Create_project_data(Get_pheno_group.out.pheno_group.collect(), Create_inittable.out.init_table_channel_no_ldsc)
            }   
        }
}
