
process Create_inittable_LDSC {
    publishDir "${params.output_folder}/init_table_${params.ancestry}_${params.current_date}/", pattern: "*.hdf5", mode: 'copy'
    input:
        path cleaned_gwas_chr
        path cleaned_gwas
        path Covariance_H0
        path Covariance_gen
        path Regions
    output:
        path "*.hdf5", emit: init_table
    """

    gwas_list_str=`echo ${cleaned_gwas} | sed s'/.txt//'g`
    echo \$gwas_list_str
    date_init=\$(date +"%m_%d_%Y-%H:%M")

    init_name="inittable_LDSC_\$date_init.hdf5"
    jass create-inittable --input-data-path "./*.txt" --init-covariance-path ${Covariance_H0} --init-genetic-covariance-path ${Covariance_gen} --regions-map-path ${Regions} --description-file-path ${params.meta_data} --init-table-path  \$init_name

    """
}

process Create_inittable {
    publishDir "${params.output_folder}/init_table_${params.ancestry}_${params.current_date}/", pattern: "*.hdf5", mode: 'copy'
    input:
        path cleaned_gwas_chr
        path cleaned_gwas
        path Regions
    output:
        path "*.hdf5", emit: init_table_channel_no_ldsc
    """
    gwas_list_str=`echo ${cleaned_gwas} | sed s'/.txt//'g`
    echo \$gwas_list_str
    date_init=\$(date +"%m_%d_%Y-%H:%M")

    init_name="inittable_NO_LDSC_\$date_init.hdf5"
    jass create-inittable --input-data-path "./*.txt" --regions-map-path ${Regions} --description-file-path ${params.meta_data} --init-table-path \$init_name

    """
}

//
//   Generate one list of phenotype
//
process Get_pheno_group {
    input:
        path group
    output:
        path "pheno_group_*", emit: pheno_group
    """
        split -l 1 ${group} pheno_group_
    """
}

process Create_project_data {
    publishDir "${params.output_folder}/worktable_${params.ancestry}_${params.current_date}/", pattern:"worktable_*.hdf5", mode: 'copy'
    publishDir "${params.output_folder}/quadrant/", pattern:"quadrant_*.png", mode: 'copy'
    publishDir "${params.output_folder}/manhattan/", pattern:"manhattan_*.png", mode: 'copy'
    input:
        path pheno_group
        path init_table
    output:
        path "worktable_*.hdf5", emit: worktables
        path "quadrant_*.png", emit: quadrant
        path "manhattan_*.png", emit: manhattan
    """
        for pheno_line in ${pheno_group}
        do
            echo \$pheno_line
            group_tag="\$(cat \$pheno_line | cut -d';' -f1)"
            pheno_list="\$(cat \$pheno_line | cut -d';' -f2)"

            echo \$group_tag
            echo \$pheno_list

            jass create-project-data --phenotypes \$pheno_list --init-table-path ./${init_table} --worktable-path ./worktable_bis_\$group_tag.hdf5 --manhattan-plot-path ./manhattan_\$group_tag.png --quadrant-plot-path ./quadrant_\$group_tag.png
        done
    """
}
