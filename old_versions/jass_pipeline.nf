nextflow.enable.dsl=1
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                           JASS suite pipeline
          Authors : Hanna Julienne, Hervé Ménager & Lucie Troubat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

/* Parameter to set if optional pipeline steps are performed */
params.compute_project=true // Compute JASS runs
params.compute_LDSC_matrix=false // Infer the genetic covariance and residual covariance using the LDscore regression (Bulik-Sullivan, et al, 2015). The residual covariance is necessary to perform multi-trait GWAS (see julienne, et al 2021) If set to false, the residual covariance will be infered from Zscores
params.compute_imputation=false

/* Path of input data */
params.meta_data = "${projectDir}"+"/input_files/Data_test_EAS.csv" // file describing gwas summary statistic format
params.gwas_folder = "/pasteur/zeus/projets/p02/GGS_JASS/1._DATA/Summary_stat_hg38/SAS/" 

params.region = "${projectDir}"+"/input_files/All_Regions_ALL_ensemble_1000G_hg38_SAS.bed"
params.ref_panel_WG = "${projectDir}"+"/Ref_Panel/1000G_SAS_0_01.csv"//"${projectDir}/Ref_Panel/1000G_SAS_0_01_chr22.csv"

params.ancestry="SAS"
params.prefix="ALL_ensemble_1000G_hg38_SAS_chr"
params.prefix_Impute_GWAS="ALL_ensemble_1000G_hg38_SAS_"
params.suffix=""
params.LD_SCORE_folder='/pasteur/zeus/projets/p02/GGS_WKD/DATA_1000G/Panels/Matrix_LDSCORE/SAS/'

/* Folder to store result output */
params.output_folder = "${projectDir}"
diagnostic_folder= params.output_folder + "/sample_size/"

/**********************/
/* Process Parameters */
/**********************/
/* Imputation parameter (for RAISS) */
/* If you decide to run RAISS, we strongly encourage you to tune 
    the following parameters to your data to ensure imputation accuracy.
    see https://statistical-genetics.pages.pasteur.fr/raiss/#optimizing-raiss-parameters-for-your-data
*/

params.r2threshold = 0.6
params.eigenthreshold = 0.05 
params.minimumld = 5
params.ld_folder="/pasteur/zeus/projets/p02/GGS_WKD/DATA_1000G/Panels/Matrix_LD_RAISS/SAS/*.ld"
params.ref_panel = '/pasteur/zeus/projets/p02/GGS_WKD/DATA_1000G/Panels/SAS/'

/* Project group */
params.group = "${projectDir}/input_files/group.txt"
group = file(params.group)

/* Generate channel from path */
Region_channel = Channel.fromPath(params.region)
Region_channel2 = Channel.fromPath(params.region)

chr_channel = Channel.from(21..22)
ref_chr_path=params.ref_panel+"/ALL_ensemble_1000G_hg38_SAS_chr*.bim"


extract_sample_size_script_channel = Channel.fromPath("${projectDir}/bin/extract_sample_size.py")
generate_trait_pairs_channel = Channel.fromPath("${projectDir}/bin/generate_trait_pairs.py")
parse_correlation_channel = Channel.fromPath("${projectDir}/bin/parse_correlation_results.py")


process meta_data_GWAS{
    output:
        path "meta_data_chk*.csv" into meta_data mode flatten
    """
    d=`wc -l ${params.meta_data}`
    e=`echo \$d | cut -d ' ' -f 1`

    for ((i = 2; i <= \$e; i++));
    do
        head -n1 ${params.meta_data} > "meta_data_chk\$i.csv"
        head -n \$i ${params.meta_data} | tail -n 1 >> "meta_data_chk\$i.csv"  
    done
    """
}

process Clean_GWAS {
    publishDir "${params.output_folder}/harmonized_GWAS_files/", pattern: "*.txt", mode: 'copy'
    publishDir "${params.output_folder}", pattern: "harmonized_GWAS_1_file/*.txt", mode: 'copy'
    input:
        path ref_panel from params.ref_panel_WG 
        path meta_chunk from meta_data
    output:
        path "harmonized_GWAS_1_file/*.txt" into cleaned_gwas_channel
        path "harmonized_GWAS_1_file/*.txt" into cleaned_gwas_channel2
        path "harmonized_GWAS_1_file/*.txt" into cleaned_gwas_channel3
        path "*.txt" into cleaned_gwas_chr_channel mode flatten
        path "*.txt" into cleaned_gwas_chr_channel2 mode flatten
    """
    mkdir -p harmonized_GWAS_1_file
    pwd
    ls ${params.gwas_folder}
    echo ${params.gwas_folder}
    working_dir=\$(pwd)
    echo \$working_dir
    full_path=\$working_dir/${meta_chunk}
    echo "full_path"
    echo \$full_path

    jass_preprocessing --gwas-info \$full_path --ref-path ${ref_panel} \
        --input-folder ${params.gwas_folder} --diagnostic-folder ${diagnostic_folder} \
        --output-folder ./ --output-folder-1-file harmonized_GWAS_1_file/
    """
}


process Impute_GWAS {
    publishDir "${params.output_folder}", pattern: "imputed_GWAS/*.txt", mode: 'copy'
    input:
        path gwas_files from cleaned_gwas_chr_channel
        path ref_file from Channel.fromPath(ref_chr_path).collect()
        path ld_file from Channel.fromPath(params.ld_folder).collect()
    output:
        path "imputed_GWAS/*.txt" into imputed_gwas_channel
        path "imputed_GWAS/*.txt" into imputed_gwas_channel2
    when:
        params.compute_imputation
    script:
    """
    mkdir -p imputed_GWAS

    chrom=\$(echo ${gwas_files} | cut -d '_' -f4 | cut -d "." -f1)
    study=\$(echo ${gwas_files} | cut -d '_' -f2,3)

    echo \$chrom
    echo \$study

    raiss --chrom \${chrom} --gwas \${study} --ref-folder ./ --R2-threshold ${params.r2threshold} --eigen-threshold ${params.eigenthreshold} --ld-folder ./ --zscore-folder ./ --output-folder ./imputed_GWAS --ref-panel-prefix ${params.prefix_Impute_GWAS} --ref-panel-suffix ${params.suffix}.bim --minimum-ld ${params.minimumld}
    """
}

process Do_not_Impute_GWAS {
    queue 'dedicated,common,ggs'
    input:
        path gwas_files from cleaned_gwas_chr_channel2.collect()
    output:
        path "clean_gwas/*.txt" into not_imputed_gwas_channel
        path "clean_gwas/*.txt" into not_imputed_gwas_channel2
    when:
        !params.compute_imputation
    script:
    """
    if [ ! -d "clean_gwas" ]
    then
        mkdir clean_gwas
    fi
    cp -L *.txt ./clean_gwas

    """
}

/*
    Process related to LD-score calculation
*/
process Munge_LDSC_data {
    publishDir "${params.output_folder}", pattern: "ldsc_data/data_*.sumstats.gz", mode: 'copy'
    publishDir "${params.output_folder}", pattern: "ldsc_data/*.log", mode: 'copy'
    time = {1.h * task.attempt}
    maxRetries = 4
    queue 'dedicated,common,ggs'
    //errorStrategy='ignore'
    input:
        path clean_gwas from cleaned_gwas_channel.flatten()

    output:
        path "ldsc_data/*.sumstats.gz" into ldsc_data_channel
        path "ldsc_data/*.sumstats.gz" into ldsc_data_channel_bis
        path "ldsc_data/*.sumstats.gz" into ldsc_data_channel_three
    when:
        params.compute_LDSC_matrix
    """
    cp ${projectDir}/bin/extract_sample_size.py ./
    Nsamp=\$(python2.7 extract_sample_size.py ${clean_gwas} ${params.meta_data})

    if [ ! -d "ldsc_data" ]
    then
        mkdir ldsc_data
    fi
    tag=\$(echo ${clean_gwas} | cut -d '.' -f1 | cut -d '_' -f2,3)
    echo \$tag
    echo \$Nsamp
    cat $clean_gwas | sed '1s/A1/A2/g' | sed '1s/A0/A1/g' > corrected_gwas.txt

    head corrected_gwas.txt
    munge_sumstats.py --sumstats corrected_gwas.txt --N \$Nsamp --out ldsc_data/data_\$tag
    """
}

process Generate_trait_pair {
    time '1h'
    queue 'dedicated,common,ggs'
    input:
        path generate_trait_pairs_script from generate_trait_pairs_channel
        path ldsc_data from ldsc_data_channel.unique().collect()
    output:
        path "pairs_chunk_*.txt" into combi_channel mode flatten
    when:
        params.compute_LDSC_matrix
    """
    python3 ${generate_trait_pairs_script}
    """
}

process heritability_LDSC_data {
    memory {8.GB * task.attempt}
    time {24.h * task.attempt}
    queue 'dedicated,common,ggs'
    publishDir "${params.output_folder}/h2_data/", pattern: "*.log", mode: 'copy'
    input:
        path ldsc_data from ldsc_data_channel_three.flatten()
    output:
        path "*.log" into h2_log_channel
    when:
        params.compute_LDSC_matrix
    """
    my_trait=\$(ls ${ldsc_data} | cut -d "_" -f2,3 | cut -d '.' -f1)
    ldsc.py --h2 ${ldsc_data} --ref-ld-chr ${params.LD_SCORE_folder}${params.prefix}@ --w-ld-chr ${params.LD_SCORE_folder}${params.prefix}@ --out \${my_trait}
    """
}


process Correlation_LDSC_data {
    memory {8.GB * task.attempt}
    time {24.h * task.attempt}
    queue 'dedicated,common,ggs'
    publishDir "${params.output_folder}/cor_data/", pattern: "*.log", mode: 'copy'
    input:
        path trait_pair from combi_channel
        path ldsc_data from ldsc_data_channel_bis.unique().collect()
    output:
        path "*.log" into cor_log_channel
    when:
        params.compute_LDSC_matrix

    """

    echo ${trait_pair}
    IFS=';' read -ra my_trait <<< "\$(cat ${trait_pair})"
    i=1

    for trait_p in \${my_trait[@]}
    do

        echo \$trait_p
        trait1=\$(echo \$trait_p | cut -d '.' -f1 | cut -d '_' -f2,3)
        trait2=\$(echo \$trait_p | cut -d ',' -f2 | cut -d '.' -f1 | cut -d '_' -f2,3)

        ldsc.py --rg \$trait_p --out \${trait1}-_-\${trait2} --ref-ld-chr ${params.LD_SCORE_folder}${params.prefix}@ --w-ld-chr ${params.LD_SCORE_folder}${params.prefix}@

    done
    """
}


process Correlation_matrices {

    publishDir "${params.output_folder}/Correlation_matrices/", pattern: "*.csv", mode: 'copy'
    time '1h'
    queue 'dedicated,common,ggs'
    input:
        path parsing_script from parse_correlation_channel
        path ldsc_data from cor_log_channel.collect()
        path h2_ld from h2_log_channel.collect()
    output:
        path "Covariance_matrix_H0.csv" into cov_H0_matrice_channel
        path "Covariance_matrix_genetic.csv" into cov_gen_matrice_channel
        path "*.csv" into parsing_results
    when:
        params.compute_LDSC_matrix
    """
    python3 ${parsing_script}

    """
}


process Create_inittable_LDSC {
    publishDir "${params.output_folder}/init_table/", pattern: "*.hdf5", mode: 'copy'
    memory {16.GB * task.attempt}
    time {24.h * task.attempt}
    queue 'dedicated,common,ggs'
    input:
        path cleaned_gwas_chr from imputed_gwas_channel.mix(not_imputed_gwas_channel).unique().collect()
        path cleaned_gwas from cleaned_gwas_channel2.collect()
        path Covariance_H0 from cov_H0_matrice_channel
        path Covariance_gen from cov_gen_matrice_channel
        path Regions from Region_channel
    output:
        path "*.hdf5" into init_table_channel
    when:
        params.compute_LDSC_matrix
    """

    gwas_list_str=`echo ${cleaned_gwas} | sed s'/.txt//'g`
    echo \$gwas_list_str
    date_init=\$(date +"%m_%d_%Y-%H:%M")

    init_name="inittable_LDSC_\$date_init.hdf5"
    jass create-inittable --input-data-path "./*.txt" --init-covariance-path ${Covariance_H0} --init-genetic-covariance-path ${Covariance_gen} --regions-map-path ${Regions} --description-file-path ${params.meta_data} --init-table-path  \$init_name

    """
}


process Create_inittable {

    publishDir "${params.output_folder}/init_table/", pattern: "*.hdf5", mode: 'copy'
    memory {16.GB * task.attempt}
    time {24.h * task.attempt}

    input:
        path cleaned_gwas_chr from imputed_gwas_channel2.mix(not_imputed_gwas_channel2).unique().collect()
        path cleaned_gwas from cleaned_gwas_channel3.collect()
        path Regions from Region_channel2
    output:
        path "*.hdf5" into init_table_channel_no_ldsc
    when:
        !params.compute_LDSC_matrix
    """

    gwas_list_str=`echo ${cleaned_gwas} | sed s'/.txt//'g`
    echo \$gwas_list_str
    date_init=\$(date +"%m_%d_%Y-%H:%M")

    init_name="inittable_NO_LDSC_\$date_init.hdf5"
    jass create-inittable --input-data-path "./*.txt" --regions-map-path ${Regions} --description-file-path ${params.meta_data} --init-table-path \$init_name

    """
}


/*
    Generate one list of phenotype
*/
process get_pheno_group {
    input:
        path 'group.txt' from group
    output:
        path "pheno_group_*" into pheno_group_channel
    when:
        params.compute_project
    """
    split -l 1 group.txt pheno_group_
    """
}

process Create_project_data {
    publishDir "${projectDir}/worktable/", pattern:"worktable_*.hdf5", mode: 'move'
    publishDir "${projectDir}/quadrant/", pattern:"quadrant_*.png", mode: 'move'
    publishDir "${projectDir}/manhattan/", pattern:"manhattan_*.png", mode: 'move'

    input:
        path pheno_group from pheno_group_channel.collect()
        path init_table from init_table_channel.mix(init_table_channel_no_ldsc)

    output:
        path "worktable*.hdf5" into worktable_channel
        path "*.png" into plot_channel
        stdout result

    when:
        params.compute_project
    """
        for pheno_line in ${pheno_group}
        do
            echo \$pheno_line
            group_tag="\$(cat \$pheno_line | cut -d';' -f1)"
            pheno_list="\$(cat \$pheno_line | cut -d';' -f2)"

            echo \$group_tag
            echo \$pheno_list

            jass create-project-data --phenotypes \$pheno_list --init-table-path ./${init_table} --worktable-path ./worktable_bis_\$group_tag.hdf5 --manhattan-plot-path ./manhattan_\$group_tag.png --quadrant-plot-path ./quadrant_\$group_tag.png
        done
    """
}
//result.println { it.trim() }
