dag {
    enabled = true
    overwrite = true
    file = 'dag.dot'
}

report {
       enabled = true
      overwrite = true
       file = 'nextflow_logs/report.html'
}

trace {
        enabled = true
        overwrite = true
        file = 'nextflow_logs/trace.txt'
}

singularity {
            enabled = true
            autoMounts = true
            runOptions = '--home $HOME:/home/$USER -B /pasteur/zeus/projets/p02/'
}
executor {
    name = 'slurm'

    
    queueSize = 400

}

process{
    errorStrategy = 'ignore'
    maxErrors = 20
    maxRetries = 2

    withName: 'Compute_MAF' {
      container='plink_1.90b6.21--h516909a_0.sif'
      time='1h'
      queue = "ggs,common"
      cpus=1
    }

    withName: 'Create_WG_reference_panel' {
    memory = '32G'
    time='1h'
    queue = "ggs,common"
		cpus=1
	  }

    withName: 'Meta_data_GWAS' {
      qos='ultrafast'
      queue = "ggs,common"
      cpus=1
    }

    withName: 'Clean_GWAS' {
      memory = '32G'
      time='2h'
      queue = "ggs,common"
      cpus=1
    }

    withName: 'Impute_GWAS' {
        memory={8.GB * task.attempt}
        errorStrategy = 'ignore'
        time='24h' 
        maxRetries=4
        queue = "ggs,common" 
        cpus=1
    }

    withName: 'Perf_RAISS' {
        memory={8.GB * task.attempt}
        time='24h'
        maxRetries=4
        queue = "ggs,common"
        cpus=1
    }

    withName: 'Munge_LDSC_data' {
      container='ldsc_1.0.1--py_0.sif'
      cpus=1
      time={1.h * task.attempt}
      maxRetries=4
      queue = "ggs,common"
    }

    withName: 'Generate_trait_pair' {
      
      container='jass_preprocessing_2.2--pyhdfd78af_0.sif'
      queue = "ggs,common"
      cpus=1
    }

    withName: 'Heritability_LDSC_data' {
      memory={8.GB * task.attempt}
      time={2.h * task.attempt}
      queue = "ggs,common"
      container="ldsc_1.0.1--py_0.sif"
      cpus=1
    }

    withName: 'Correlation_LDSC_data' {
      container="ldsc_1.0.1--py_0.sif"
      queue = "ggs,common"
      cpus=1
    }

    withName: 'Parsing_correlation_matrices' {
      container='jass_preprocessing_2.2--pyhdfd78af_0.sif'
      qos='ultrafast'
      queue = "ggs,common"
      cpus=1
    }

    withName: 'Create_inittable_LDSC' {
      memory={16.GB * task.attempt}
      queue = "ggs,common"
      time={2.h * task.attempt}
      cpus=1
    }

    withName: 'sanity_cleaned_in_folder' {
      cpus=1
      qos='ultrafast'
    }

    withName: 'sanity_imputed_in_folder' {
      cpus=1
      qos='ultrafast'
    }

    withName: 'Create_inittable' {
      memory={16.GB * task.attempt}
      queue = "ggs,common"
      time={2.h * task.attempt}
      cpus=1
    }

    withName: 'get_pheno_group' {
      cpus=1
      qos='ultrafast'
    }

    withName: 'Create_project_data' {
    cpus=1
    queue = "ggs,common"
    time={2.h * task.attempt}
    }
  
}
