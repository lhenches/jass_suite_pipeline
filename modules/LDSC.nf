
process Munge_LDSC_data {
    publishDir "${params.output_folder}", pattern: "ldsc_data_${params.ancestry}_${params.current_date}/data_*.sumstats.gz", mode: 'copy'
    publishDir "${params.output_folder}", pattern: "ldsc_data_${params.ancestry}_${params.current_date}/*.log", mode: 'copy'

    input:
        path clean_gwas
    output:
        path "ldsc_data/*.sumstats.gz", emit: ldsc_data_channel
    """
    cp ${projectDir}/bin/extract_sample_size.py ./
    Nsamp=\$(python2.7 extract_sample_size.py ${clean_gwas} ${params.meta_data})

    if [ ! -d "ldsc_data" ]
    then
        mkdir ldsc_data
    fi
    tag=\$(echo ${clean_gwas} | cut -d '.' -f1 | cut -d '_' -f2,3)
    echo \$tag
    echo \$Nsamp
    cat $clean_gwas | sed '1s/A1/A2/g' | sed '1s/A0/A1/g' > corrected_gwas.txt

    head corrected_gwas.txt
    munge_sumstats.py --sumstats corrected_gwas.txt --N \$Nsamp --out ldsc_data/data_\$tag
    """
}

process Heritability_LDSC_data {

    publishDir "${params.output_folder}/h2_data_${params.ancestry}_${params.current_date}/", pattern: "*.log", mode: 'copy'
    input:
        path ldsc_data
    output:
        path "*.log", emit: h2_log_channel
    """
    my_trait=\$(ls ${ldsc_data} | cut -d "_" -f2,3 | cut -d '.' -f1)
    ldsc.py --h2 ${ldsc_data} --ref-ld-chr ${params.LD_SCORE_folder}${params.prefix}@ --w-ld-chr ${params.LD_SCORE_folder}${params.prefix}@ --out \${my_trait}
    """
}


process Generate_trait_pair {
    time '1h'
    input:
        path generate_trait_pairs_script
        path ldsc_data
    output:
        path "pairs_chunk_*.txt",emit: combi_channel
    """
        python3 ${generate_trait_pairs_script}
    """
}


process Correlation_LDSC_data {
    memory {8.GB * task.attempt}
    time {24.h * task.attempt}

    publishDir "${params.output_folder}/cor_data_${params.ancestry}_${params.current_date}/", pattern: "*.log", mode: 'copy'
    input:
        path trait_pair
        path ldsc_data 
    output:
        path "*.log", emit: cor_log_channel
    """

    echo ${trait_pair}
    IFS=';' read -ra my_trait <<< "\$(cat ${trait_pair})"
    i=1

    for trait_p in \${my_trait[@]}
    do

        echo \$trait_p
        trait1=\$(echo \$trait_p | cut -d '.' -f1 | cut -d '_' -f2,3)
        trait2=\$(echo \$trait_p | cut -d ',' -f2 | cut -d '.' -f1 | cut -d '_' -f2,3)

        ldsc.py --rg \$trait_p --out \${trait1}-_-\${trait2} --ref-ld-chr ${params.LD_SCORE_folder}${params.prefix}@ --w-ld-chr ${params.LD_SCORE_folder}${params.prefix}@

    done
    """
}


process Parsing_correlation_matrices {
    publishDir "${params.output_folder}/Correlation_matrices/", pattern: "*.csv", mode: 'copy'
    time '1h'
    input:
        path parsing_script
        path ldsc_data
        path h2_ld
    output:
        path "Covariance_matrix_H0_${params.ancestry}_${params.current_date}.csv", emit: cov_H0_matrice_channel
        path "Covariance_matrix_genetic_${params.ancestry}_${params.current_date}.csv", emit: cov_gen_matrice_channel
        path "*.csv", emit: parsing_results
    when:
        params.compute_LDSC_matrix
    """
    python3 ${parsing_script} ${params.ancestry} ${params.current_date}

    """
}


process Make_HeatMap {
    publishDir "${params.output_folder}/heatmap/", pattern: "*.png", mode: 'copy'
    time '1h'
    input:
        path make_heatmap_script
        path matrix_csv_files

    output:
        path "*.png", emit: heatmap_channel

    when:
        params.compute_LDSC_matrix

    """
        Rscript ${make_heatmap_script} Correlation_matrix_genetic_${params.ancestry}_${params.current_date}.csv Pval_cor_matrix_genetic_${params.ancestry}_${params.current_date}.csv

    """

}
