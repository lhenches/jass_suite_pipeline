
process Meta_data_GWAS{
    input:
        path pheno_list
    output:
        path "meta_data_chk*.csv"
    """
    e=\$(grep -c '' ${pheno_list})

    for ((i = 2; i <= \$e; i++));
    do
        head -n1 ${pheno_list} > "meta_data_chk\$i.csv"
        head -n \$i ${pheno_list} | tail -n 1 >> "meta_data_chk\$i.csv"  
    done
    """
}

process Clean_GWAS{
    publishDir "${params.output_folder}/harmonized_GWAS_files_${params.ancestry}_${params.current_date}/", pattern: "*.txt", mode: 'copy'
    publishDir "${params.output_folder}", pattern: "harmonized_GWAS_1_file_${params.ancestry}_${params.current_date}/*.txt", mode: 'copy'
    input:
        path ref_panel
        path meta_chunk
    output:
        path "harmonized_GWAS_1_file_${params.ancestry}_${params.current_date}/*.txt", emit: cleaned_gwas_channel
        path "*.txt", emit: cleaned_gwas_chr_channel
    """
    mkdir -p harmonized_GWAS_1_file_${params.ancestry}_${params.current_date}
    pwd
    ls ${params.gwas_folder}
    echo ${params.gwas_folder}
    working_dir=\$(pwd)
    echo \$working_dir
    full_path=\$working_dir/${meta_chunk}
    echo "full_path"
    echo \$full_path

    jass_preprocessing --gwas-info \$full_path --ref-path ${ref_panel} \
        --input-folder ${params.gwas_folder} --diagnostic-folder ${params.diagnostic_folder} \
        --output-folder ./ --output-folder-1-file harmonized_GWAS_1_file_${params.ancestry}_${params.current_date}/
    """
}
